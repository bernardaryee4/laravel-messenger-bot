<?php

namespace App\Console\Commands;

use App\Helpers\HttpHelper;
use Illuminate\Console\Command;

class SetGreetingText extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:greeting:set {text} {--locale=default}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets the greeting text for our bot';

    protected $httpHelper;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->httpHelper = new HttpHelper();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = [
            "greeting" => [
                [
                    "locale" => $this->option("locale"),
                    "text" => $this->argument("text")
                ]
            ]
        ];

        $this->httpHelper->post(env("FACEBOOK_PROFILES_API")."?access_token=".env("PAGE_ACCESS_TOKEN"),$data);

    }
}
