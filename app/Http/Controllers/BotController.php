<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Helpers\HttpHelper;

class BotController extends Controller{


    public function __construct()
    {
        //initialize HttpHelper
        $this->httpHelper = new HttpHelper();
    }
    private $httpHelper;

    /**
     *
     * This is the entry point for messenger webhook requests
     *
     * @param Request $request --- Request sent from messenger
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function receiver(Request $request){

        Log::info("Received request : ". $request);

        Log::info("About to extract and dispatch messages to queue ");
        $entries = \App\Bot\Webhook\Entry::getEntries($request);

        //Lets extract the messaging payload from the requests
        foreach ($entries as $entry) {
            $messagings = $entry->getMessagings();
            foreach ($messagings as $messaging) {
                dispatch(new \App\Jobs\BotHandler($messaging));
            }
        }

        Log::info("Done dispatching messages to queue");

        //Return OK response to messenger
        return response('',200);
    }
}
