<?php


namespace App\Bot;


class QuickLinks
{
    const quickReplies =   array(
            array(
                'content_type' => 'text',
                'title' => 'Business',
                'payload' => 'business'
            ),
            array(
                'content_type' => 'text',
                'title' => 'Entertainment',
                'payload' => 'entertainment'
            ),
            array(
                'content_type' => 'text',
                'title' => 'General',
                'payload' => 'general'
            ),
            array(
                'content_type' => 'text',
                'title' => 'Health',
                'payload' => 'health'
            ),
            array(
                'content_type' => 'text',
                'title' => 'Sports',
                'payload' => 'sports'
            ),
            array(
                'content_type' => 'text',
                'title' => 'Science',
                'payload' => 'science'
            ),
            array(
                'content_type' => 'text',
                'title' => 'Technology',
                'payload' => 'technology'
            )
        );

}
