<?php

namespace App\Bot;


use App\Helpers\HttpHelper;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use NotificationChannels\Facebook\Components\Button;
use NotificationChannels\Facebook\FacebookChannel;
use NotificationChannels\Facebook\FacebookMessage;

class NewsApi
{
    const NEWS_ITEM = "new";
    const ANSWER = "answer";
    const INTRO = "introduction";
    const QUICK_REPLY = "quick-reply";

    public $question;
    public $options;
    private $solution;
    private $userId;
    private $httpHelper;

    public function via($notifiable)
    {
        return [FacebookChannel::class];
    }

    public function __construct($data, $userId)
    {
        $this->question = "";
        $this->solution = "";
        $this->options = [];
        $this->userId = $userId;
    }


    public static function getNews($category)
    {
        $httpHelper = new HttpHelper;
        $result = $httpHelper->get(env("NEWS_API"),[
            "country"=>"us",
            "apiKey"=>env("NEWS_API_KEY"),
            "category" => $category['payload']
        ]);

        $result = json_decode($result);
//        Log::info("News items returned ". print_r($result->articles));
        return $result->articles;
    }


    public function toFacebook($notifiable)
    {
        $url = url('/invoice/' . $this->invoice->id);

        return FacebookMessage::create()
            ->to($this->user->fb_messenger_user_id) // Optional
            ->text('One of your invoices has been paid!')
            ->isUpdate() // Optional
            ->isTypeRegular() // Optional
            // Alternate method to provide the notification type.
            // ->notificationType(NotificationType::REGULAR) // Optional
            ->buttons([
                Button::create('View Invoice', $url)->isTypeWebUrl(),
                Button::create('Call Us for Support!', '+1(212)555-2368')->isTypePhoneNumber(),
            ]); // Buttons are optional as well.
    }


}
