<?php

namespace App\Bot;


use App\Bot\Webhook\Messaging;
use App\Helpers\HttpHelper;
use Illuminate\Support\Facades\Log;

class Bot
{
    private $messaging;
    private $httpHelper;

    public function __construct(Messaging $messaging)
    {
        $this->messaging = $messaging;
        $this->httpHelper = new HttpHelper;
    }


    //We extract the data from the webhook message received
    public function extractDataFromMessage()
    {
        $matches = [];

        //Let's try to get the quick Reply
        $quickReplyPayload = $this->messaging->getMessage()->getQuickReply();

        //If indeed a quick reply exists then the user's reply was a quick reply
        if (!empty($quickReplyPayload)) {
            //We extract the quick link data and return it
            return [
                "type" => NewsApi::QUICK_REPLY,
                "category" => $quickReplyPayload,
                "user_id" => $this->messaging->getSenderId()
            ];

        } else {
            // If we don't get a quick reply payload, then the user possible sent a text
            $text = $this->messaging->getMessage()->getText();
        }


        // Let's compare the text to our conversation starters
        if (preg_match("/^hi|hello|hey|heya|sup\$/i", $text, $matches)) {

            //If the user query matches ant of our conversation starters, then we say the user need and INTRO
            return [
                "type" => NewsApi::INTRO,
                "data" => [],
                "user_id" => $this->messaging->getSenderId()
            ];
        }

        //anything else, we dont care --- lol
        return [
            "type" => "unknown",
            "data" => [],
            "user_id" => $this->messaging->getSenderId()
        ];
    }



    public function reply($data)
    {
        //We use the http helper to help make requests

        //Here we are posting the feedback we think should be given to the user to facebook
        $this->httpHelper->post(
            env('FACEBOOK_MESSAGE_API').'?access_token='.env("PAGE_ACCESS_TOKEN"), $data);
    }


    public function replyWithQuickLinks($data)
    {
        //We processed quick replies a bit differently
        //We'll unify the implementation soon .. hihi
        $id = $this->messaging->getSenderId();
        $this->sendQuickLinks($id, $data);
    }


    private function sendQuickLinks($recipientId,$message){

        $messageData = array(
            'recipient' => array(
                'id' => $recipientId
            ),
            "messaging_type" => "RESPONSE",
            'message' => array(
                'text' =>"Pick a topic",
                'quick_replies' => $message
            )
        );
        $this->httpHelper->post(env('FACEBOOK_MESSAGE_API').'?access_token='.env("PAGE_ACCESS_TOKEN"), $messageData);
  }




}
