<?php

namespace App\Jobs;

use App\Bot\Bot;
use App\Bot\NewsApi;
use App\Bot\QuickLinks;
use App\Bot\Webhook\Messaging;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use NotificationChannels\Facebook\Components\Button;
use NotificationChannels\Facebook\Components\Card;
use NotificationChannels\Facebook\Enums\ButtonType;
use NotificationChannels\Facebook\Exceptions\CouldNotCreateCard;
use NotificationChannels\Facebook\Exceptions\CouldNotCreateMessage;
use NotificationChannels\Facebook\FacebookMessage;

class BotHandler implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $messaging;

    public function __construct(Messaging $messaging)
    {
        $this->messaging = $messaging;
    }

    /**
     * Execute the job.
     *
     * We broadcast the message to the recipient after processing it here
     * Messages are queued and processed in order
     * Fcebook cannot wait for "us" to finish processing our requests before returning a response to their call
     * Hence the reason why we send Facebook a 200 OK response then go ahead and process the user queries
     * @return void
     * @throws \NotificationChannels\Facebook\Exceptions\CouldNotCreateMessage
     */
    public function handle()
    {

        //Messages are of 2 types "postback" & "message"
        //Here we handle messages sent by the user
        if ($this->messaging->getType() == "message") {

            // We create a structure for the messaging object
            //Then we extract the message into the object
            $bot = new Bot($this->messaging);
            $custom = $bot->extractDataFromMessage();

            //Now that we have the type of message the person sent and their facebook ids, let's decide
            if($custom["type"] == NewsApi::INTRO){

                //Create a welcome message
                $message = FacebookMessage::create("Welcome to the News Bot, I can help you get updates on what's happening around you")
                    ->to($custom['user_id']);

                //Send the welcome message to the user
                $bot->reply($message);

                //Follow up with quick replies to help the user decide
                $bot->replyWithQuickLinks(QuickLinks::quickReplies);
            }else if($custom["type"] == NewsApi::QUICK_REPLY){

                //If the user sent a quick reply, get the category of news they want
               $newsItems = NewsApi::getNews($custom['category']);

               //Create a card out of the news items
               $cards = array();
               $count = 1;
                foreach($newsItems as $newsItem ){
                    try {
                        if($newsItem->title !=null && $newsItem->description !=null ){
                            $card = Card::create(Str::limit($newsItem->title, 10))
                                ->subtitle(Str::limit($newsItem->description, 50))
                                ->url($newsItem->url)
                                ->image($newsItem->urlToImage)
                                ->buttons([
                                    Button::create("Read More",[],ButtonType::WEB_URL)->isTypePostback()
                                ->url($newsItem->url)
                                ]);
                            if ($count++ == 10) break;
                            array_push($cards,$card);
                        }else{
                            Log::info("API returned some null fields ... Skipping content");
                        }

                    } catch (CouldNotCreateCard $e) {
                        Log::info("Could not create card :".$e->getMessage());
                    } catch (CouldNotCreateMessage $e) {
                        Log::info("Could not message:".$e->getMessage());
                    }
                }

              $message = FacebookMessage::create()
                    ->to($custom['user_id'])
                  ->cards($cards);

                //Reply with news cards
                $bot->reply($message);
            }
          else {
              //If we couldn't understand what the user said
              // === For now === Let's say we don't understand
                $message = FacebookMessage::create("I don't understand. Try \"rephrasing\" your query")
                    ->to($custom['user_id']);
                $bot->reply($message);
            }
        }
    }

}
