<?php
namespace App\Helpers;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

/**
 * Class to handle all RESTful requests
 */

class HttpHelper
{

    /**
     * HttpHelper constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param $endpoint
     * @param $array
     * @return mixed
     * @throws GuzzleException
     */
    public function post($endpoint, $params) {

        $client = new Client();
        Log::info("About to make GET request to URL : ". $endpoint);
        $response = $client->request('POST',$endpoint, [
            'headers' => [
                'Content-Type' => 'application/json; charset=UTF8',
                ],
                'json' => $params
            ]);
        Log::info("Done making request --- Status code : ".$response->getStatusCode());
        $body = $response->getBody();
        print_r($response->getBody());
        return $body;
    }



    /**
     * @param $endpoint
     * @param $params
     * @return mixed
     * @throws GuzzleException
     */
    public function get($endpoint, $params) {

        $client = new Client();
        Log::info("About to make GET request to URL : ". $endpoint);
            $response = $client->request('GET',$endpoint,
                [
                    'headers' => [
                        'Content-Type' => 'application/json; charset=UTF8',
                        'timeout' => 10,
                    ],
                    'query' => $params
                ]);
            Log::info("Done making request --- Status code : ".$response->getStatusCode());
        $body = $response->getBody();
        return $body;
    }


    public function delete($endpoint,$params) {

        $client = new Client();
        try {
            $response = $client->request('DELETE', $endpoint, [
                'headers' => [
                    'Content-Type' => 'application/json; charset=UTF8',
                    'timeout' => 10,
                ],
                'query' => $params
            ]);
        } catch (GuzzleException $e) {

        }
        $body = $response->getBody();
        return $body;
    }


    public function put($endpoint,$params) {

        $client = new Client();
        try {
            $response = $client->request('PUT', $endpoint, [
                'headers' => [
                    'Content-Type' => 'application/json; charset=UTF8',
                    'timeout' => 10,
                ],
                'query' => $params
            ]);
        } catch (GuzzleException $e) {

        }
        $body = $response->getBody();
        return $body;
    }

}
